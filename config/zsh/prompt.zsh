# Enable colors and change prompt:
autoload -U colors && colors	# Load colors
PROMPT="%F{yellow}%~ %0(?.%F{green}.%F{red})%(#.#.\$) "
setopt autocd		# Automatically cd into typed directory.
stty stop undef		# Disable ctrl-s to freeze terminal.
setopt interactive_comments
